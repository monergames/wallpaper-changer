package com.mg.wallpaperchanger;
 
import com.unity3d.player.UnityPlayerActivity;
import android.content.Context;
import android.os.Bundle;
import android.util.Config;
import android.util.Log;
import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class WallpaperActivity extends UnityPlayerActivity
{
    private static final String TAG = "WallpaperActivity_Unity";
    private static Context context;
    private static int number = 0;
 
    @Override
    protected void onCreate(Bundle myBundle) {
        super.onCreate(myBundle);
        context = getApplicationContext();
    }
    @Override
    protected void onResume() {
        if (Config.DEBUG)
            Log.d(TAG, "onResume");
        super.onResume();
    }
    @Override
    protected void onPause()
    {
        super.onPause();
    }
    @Override
    protected void onStop() {
        if (Config.DEBUG)
            Log.d(TAG, "onStop");
        super.onStop();
    }
    public static int getNumber()
    {
    	number++;
    	return number;
    }
    public static void Set(String imgPath)
    {
		Log.d(TAG,  "======== Wallpaper.Set: " + imgPath + " ========");
		WallpaperManager wpm = WallpaperManager.getInstance(context);
		InputStream ins = null;
		try {
			ins = new URL(imgPath).openStream();
		} catch (MalformedURLException e1) {
	        Log.d(TAG, "=================ERROR1===================" + e1.toString() + " ==================ERROR==================");
			e1.printStackTrace();
		} catch (IOException e1) {
	        Log.d(TAG, "=================ERROR2===================" + e1.toString() + " ==================ERROR==================");
			e1.printStackTrace();
		}
		try {
			wpm.setStream(ins);
		} catch (IOException e) {
			e.printStackTrace();
	        Log.d(TAG, "=================ERROR0===================" + e.toString() + " ==================ERROR==================");
		}
    	/*File f1 = new File(imgPath);
    	if(f1.exists()) {
    	    Bitmap bmp = BitmapFactory.decodeFile(imgPath);
    	    BitmapDrawable bitmapDrawable = new BitmapDrawable(bmp);
    	    WallpaperManager m=WallpaperManager.getInstance(context);

    	    try {
    	        m.setBitmap(bmp);
    	    } catch (IOException e) {
    	        e.printStackTrace();
    	        Log.d(TAG, "=================ERROR===================" + e.toString() + " ==================ERROR==================");
    	    }
    	} else {
    		Log.d(TAG,  "====================== Image file doesn't exist: " + imgPath + " ======================");
    	}*/
    }
}