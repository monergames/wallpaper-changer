﻿using UnityEngine;

public class WallpaperChangerExample : MonoBehaviour
{
    string debug_output = "(debug output)";

	void OnGUI()
    {
        GUI.matrix = Matrix4x4.TRS(Vector3.one, Quaternion.identity, Vector3.one * 4); 

        if (GUILayout.Button("Set test wallpaper"))
        {
            string path = System.IO.Path.Combine(Application.streamingAssetsPath, "test.jpg");
            debug_output = path;
			WallpaperController.Set(path);
        }

        GUILayout.Space(50);
        GUILayout.Label(debug_output);
    }
}
