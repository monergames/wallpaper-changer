﻿using UnityEngine;
using System.Collections;
#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_WP8
using WallpaperChanger;
#endif

public class WallpaperController : MonoBehaviour
{
    public static void Set(string wallpaperImagePath)
    {
#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_WP8
		Wallpaper.Set(wallpaperImagePath, Wallpaper.Style.Stretched);
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        string applScript =
@"set theUnixPath to POSIX file ""{0}"" as text 
tell application ""Finder"" 
set desktop picture to {{theUnixPath}} as alias 
end tell";
        MonoDevelop.MacInterop.AppleScript.Run(string.Format(applScript, wallpaperImagePath));
#elif UNITY_ANDROID
        AndroidJNI.AttachCurrentThread();
        new AndroidJavaClass("com.mg.wallpaperchanger.WallpaperActivity").CallStatic("Set", wallpaperImagePath);
#elif UNITY_IOS
        Debug.Log("iOS implementation doesn't exist.");
#elif UNITY_WP8
        UnityPluginForWindowsPhone.Class1.LockHelper(wallpaperImagePath.Replace("file:///", ""));
#else
        Debug.LogWarning("Wallpaper changing is not supported on current platform.");
#endif
    }
}
